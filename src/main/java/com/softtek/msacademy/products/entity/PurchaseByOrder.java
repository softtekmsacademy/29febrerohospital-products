package com.softtek.msacademy.products.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="purchases_by_orders")
public class PurchaseByOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_purchase")
    long idPurchase;
    @Column(name = "supplier")
    String supplier;

    @Column(name = "quantity")
    int quantity;

    @Column(name = "date")
    Date date;

    @ManyToOne(cascade= CascadeType.ALL)
    @JoinColumn(name="product")
    Product product;

    public PurchaseByOrder() {
    }

    public PurchaseByOrder(String supplier, int quantity, Date date, Product product) {
        super();
        this.supplier = supplier;
        this.quantity = quantity;
        this.date = date;
        this.product = product;
    }

    public long getIdPurchase() {
        return idPurchase;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
