package com.softtek.msacademy.products.entity;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_product")
    long idProduct;

    @Column(name = "name")
    String name;

    @Column(name = "brand")
    String brand;

    @Column(name = "available_quantity")
    int availableQuantity;

    @Column(name = "price")
    float price;
    @Column(name = "description")
    String description;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "type")
    ProductType type;

    public Product() {
    }

    public Product(String name, String brand, int availableQuantity, float price, String description, ProductType type) {
        super();
        this.name = name;
        this.brand = brand;
        this.availableQuantity = availableQuantity;
        this.price = price;
        this.description = description;
        this.type = type;
    }

    public long getIdProduct() {
        return idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(int availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

}
