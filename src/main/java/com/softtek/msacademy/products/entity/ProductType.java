package com.softtek.msacademy.products.entity;

import javax.persistence.*;

@Entity
@Table(name = "products_type")
public class ProductType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_type")
    long idType;

    @Column(name = "name")
    String nameType;

    public ProductType() {
    }

    public ProductType(String nameType) {
        this.nameType = nameType;
    }

    public long getIdType() {
        return idType;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }
}
