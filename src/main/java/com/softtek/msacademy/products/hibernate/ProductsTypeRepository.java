package com.softtek.msacademy.products.hibernate;

import com.softtek.msacademy.products.entity.ProductType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductsTypeRepository extends JpaRepository<ProductType, Long> {

}
