package com.softtek.msacademy.products.hibernate;

import com.softtek.msacademy.products.entity.ProductType;
import com.softtek.msacademy.products.entity.PurchaseByOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PurchasesRepository extends JpaRepository<PurchaseByOrder, Long> {
    @Query("SELECT u FROM ProductType u where u.nameType=?1")
    ProductType getProductTypeByName(String name);
}
