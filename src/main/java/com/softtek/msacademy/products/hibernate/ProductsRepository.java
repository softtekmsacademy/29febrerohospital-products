package com.softtek.msacademy.products.hibernate;

import com.softtek.msacademy.products.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductsRepository extends JpaRepository<Product, Long> {
    @Query("SELECT u FROM Product u where u.idProduct = ?1")
    Product getProductById(long id);

    @Query("SELECT u FROM Product u where u.availableQuantity > 0")
    List<Product> getAvailabilityProducts();
}
