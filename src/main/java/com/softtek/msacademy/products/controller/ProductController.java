package com.softtek.msacademy.products.controller;


import com.softtek.msacademy.products.entity.Product;
import com.softtek.msacademy.products.entity.ProductType;
import com.softtek.msacademy.products.entity.PurchaseByOrder;
import com.softtek.msacademy.products.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
public class ProductController {

    @Autowired
    private ProductService service;

    //POST
    @PostMapping(value = "/purchases", headers = "Accept=application/json")
    public ResponseEntity<?> save(@Valid @RequestBody PurchaseByOrder purchases){
        return service.save(purchases);
    }

    //GET ALL PURCHASES
    @GetMapping(value = "/purchases")
    public List<PurchaseByOrder> getPurchases(){
        return service.getPurchases();
    }

    //GET AVAILABLES PRODUCTS
    @GetMapping(value="/products/availableProducts")
    public List<Product> getAvailability(){
        return service.getAvailability();
    }

    //GET PRODUCTS BY ID
    @GetMapping(value="/products/{id}")
    public Product getProductsById(@PathVariable long id){
        return service.getProductsById(id);
    }

    //GET ALL PRODUCTS TYPE
    @GetMapping(value = "/productsType")
    public List<ProductType> getProductsType(){
        return service.getProductsType();
    }

    //PUT
    @RequestMapping(value="/products/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@PathVariable("id") long id,@Valid @RequestBody Product product){
        return service.updateById(id,product);
    }

}