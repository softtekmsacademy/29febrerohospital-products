package com.softtek.msacademy.products.response;

import org.springframework.http.HttpStatus;

public class Response {
    private HttpStatus httpStatus;
    private String message;

    public Response(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }
}
