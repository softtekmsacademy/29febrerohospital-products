package com.softtek.msacademy.products.service;


import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.softtek.msacademy.products.entity.Product;
import com.softtek.msacademy.products.entity.ProductType;
import com.softtek.msacademy.products.entity.PurchaseByOrder;
import com.softtek.msacademy.products.hibernate.ProductsRepository;
import com.softtek.msacademy.products.hibernate.ProductsTypeRepository;
import com.softtek.msacademy.products.hibernate.PurchasesRepository;
import com.softtek.msacademy.products.response.ResponseNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.softtek.msacademy.products.response.Response;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@DefaultProperties(
        threadPoolProperties = {
                @HystrixProperty(name = "coreSize", value = "8"),
                @HystrixProperty(name = "coreSize", value = "10"),
                @HystrixProperty(name = "maxQueueSize", value = "-1")
        },
        commandProperties = {
                @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "20"),
                @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
                @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000"),
                @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "10000"),
                @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "10")
        }
)
public class ProductService {

    @Autowired
    private PurchasesRepository purchaseService;

    @Autowired
    private ProductsRepository productService;

    @Autowired
    private ProductsTypeRepository productTypeService;

    //POST
    @HystrixCommand(fallbackMethod = "createPurchaseBreaker")
    public ResponseEntity save(PurchaseByOrder purchases){
        ProductType product = purchaseService.getProductTypeByName(purchases.getProduct().getType().getNameType());
        if(product == null){
            return new ResponseEntity<String>("This kind of product doesn´t exist", HttpStatus.BAD_REQUEST);
        }else{
            purchases.getProduct().setType(product);
            purchaseService.save(purchases);
            return new ResponseEntity<>(new Response(HttpStatus.CREATED ,"Purchase added succesfully"), HttpStatus.CREATED);
        }
    }

    //GET ALL
    @HystrixCommand(fallbackMethod = "getAllBreaker")
    public List<PurchaseByOrder> getPurchases(){
        return purchaseService.findAll();
    }

    //GET AVAILABLES PRODUCTS
    @HystrixCommand(fallbackMethod = "getAvailableBreaker")
    public List<Product> getAvailability(){
        return productService.getAvailabilityProducts();
    }

    //GET BY ID
    @HystrixCommand(fallbackMethod = "getProductsBreaker")
    public Product getProductsById(long id){
        return productService.findById(id).orElseThrow(() -> new ResponseNotFound(" Product","id",id));
    }

    //GET ALL PRODUCTS TYPE
    @HystrixCommand(fallbackMethod = "getProductsTypeBreaker")
    public List<ProductType> getProductsType(){
        return productTypeService.findAll();
    }

    //PUT
    @HystrixCommand(fallbackMethod = "updateProductBreaker")
    public ResponseEntity<?> updateById(@PathVariable long id,@Valid @RequestBody Product product){
        Product products = productService.getProductById(id);

        ProductType type = purchaseService.getProductTypeByName(product.getType().getNameType());

        products.setName(product.getName());
        products.setBrand(product.getBrand());
        products.setAvailableQuantity(product.getAvailableQuantity());
        products.setPrice(product.getPrice());
        products.setDescription(product.getDescription());
        products.setType(type);

        productService.save(products);

        return new ResponseEntity<>(new Response(HttpStatus.ACCEPTED ,"Product updated succesfully"), HttpStatus.ACCEPTED);
    }

    //FALLBACK METHODS
    private ResponseEntity<?> createPurchaseBreaker(PurchaseByOrder purchase){
        return new ResponseEntity<>(new Response(HttpStatus.REQUEST_TIMEOUT, "Error trying to add. Time out"), HttpStatus.REQUEST_TIMEOUT);
    }

    private ResponseEntity<?> updateProductBreaker(long id, Product product){
        return new ResponseEntity<>(new Response(HttpStatus.REQUEST_TIMEOUT, "Failed to trying to update, timeout."), HttpStatus.REQUEST_TIMEOUT);
    }

    private Product getProductsBreaker(long id){
        return new Product();
    }

    private List<PurchaseByOrder> getAllBreaker (){
        return new ArrayList<>();
    }

    private List<Product> getAvailableBreaker (){
        return new ArrayList<>();
    }

    private List<ProductType> getProductsTypeBreaker (){
        return new ArrayList<>();
    }

}
